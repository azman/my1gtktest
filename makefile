# makefile to compile gtk-based project

TARGET = gtktest
SOURCE = window_main.h
SOURCE += my1gtkapp.o main.o

# nice to have this... my1codelib project
EXTPATH = ../my1codelib/src

CC = gcc

CFLAGS = $(shell pkg-config --cflags gtk+-3.0)
LDLIBS = $(shell pkg-config --libs gtk+-3.0) -export-dynamic
CFLAGS += -Wall -I.

# specify extra header location
CFLAGS += -I$(EXTPATH)/

$(TARGET): $(SOURCE)
	$(CC) $(CFLAGS) -o $(TARGET) $(SOURCE) $(LDLIBS)

all: $(TARGET)

new: clean all

%.o: src/%.c src/%.h
	$(CC) -c $(CFLAGS) -o $@ $<

%.o: src/%.c
	$(CC) -c $(CFLAGS) -o $@ $<

%.h: glade/%.glade
	bash buildh $@

# my1codelib include
%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) $(CFLAGS) -c $<

clean:
	rm -rf $(TARGET) $(SOURCE) *.o

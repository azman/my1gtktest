/*----------------------------------------------------------------------------*/
#ifndef __MY1GTKAPP_H__
#define __MY1GTKAPP_H__
/*----------------------------------------------------------------------------*/
#include <gtk/gtk.h>
/*----------------------------------------------------------------------------*/
#define FLAG_HEAL 0x01
/*----------------------------------------------------------------------------*/
typedef struct _my1gtkapp_t {
	GtkWidget *main, *show, *done, *heal;
	GtkCssProvider *css_more, *css_main;
	unsigned int stat, flag;
} my1gtkapp_t;
/*----------------------------------------------------------------------------*/
void gtkapp_init_data(my1gtkapp_t* gapp);
void gtkapp_free_data(my1gtkapp_t* gapp);
void gtkapp_hide_cursor(my1gtkapp_t* gapp);
void gtkapp_insert_style(GtkWidget* widget,char* classname);
void gtkapp_remove_style(GtkWidget* widget,char* classname);
gboolean on_resize(GtkWidget *widget,GtkAllocation *allocation, gpointer data);
gboolean on_key_press(GtkWidget *widget, GdkEventKey *kevent,gpointer data);
void on_button_heal_clicked(GtkWidget *widget,my1gtkapp_t* gapp);
void gtkapp_build_gui(my1gtkapp_t* gapp, char* glade_xml);
/*----------------------------------------------------------------------------*/
#endif /* __MY1GTKAPP_H__ */
/*----------------------------------------------------------------------------*/

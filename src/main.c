/*----------------------------------------------------------------------------*/
#include "my1gtkapp.h"
#include "window_main.h"
/*----------------------------------------------------------------------------*/
#include <glib/gprintf.h>
/*----------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
	my1gtkapp_t gtkapp;
	gtk_init(&argc, &argv);
	/* show something on console? */
	g_printf("GTK+ version: %d.%d.%d\n", gtk_major_version,
		gtk_minor_version, gtk_micro_version);
	g_printf("Glib version: %d.%d.%d\n", glib_major_version,
		glib_minor_version, glib_micro_version);
	/* now, we do stuff */
	gtkapp_init_data(&gtkapp);
	gtkapp_build_gui(&gtkapp,window_main);
	gtk_main();
	gtkapp_free_data(&gtkapp);
	return 0;
}
/*----------------------------------------------------------------------------*/

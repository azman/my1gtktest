/*----------------------------------------------------------------------------*/
#include "my1gtkapp.h"
/*----------------------------------------------------------------------------*/
void gtkapp_init_data(my1gtkapp_t* gapp) {
	gapp->stat = 0;
	gapp->flag = 0;
}
/*----------------------------------------------------------------------------*/
void gtkapp_free_data(my1gtkapp_t* gapp) {
}
/*----------------------------------------------------------------------------*/
void gtkapp_hide_cursor(my1gtkapp_t* gapp) {
	GdkDisplay *display = gtk_widget_get_display(gapp->main);
	GdkCursor *cursor = gdk_cursor_new_for_display(display,GDK_BLANK_CURSOR);
	GdkWindow* window = gtk_widget_get_window(gapp->main);
	gdk_window_set_cursor(window,cursor);
}
/*----------------------------------------------------------------------------*/
void gtkapp_insert_style(GtkWidget* widget,char* classname) {
	GtkStyleContext *sctx = gtk_widget_get_style_context(widget);
	if (!gtk_style_context_has_class(sctx,classname))
		gtk_style_context_add_class(sctx,classname);
}
/*----------------------------------------------------------------------------*/
void gtkapp_remove_style(GtkWidget* widget,char* classname) {
	GtkStyleContext *sctx = gtk_widget_get_style_context(widget);
	if (gtk_style_context_has_class(sctx,classname))
		gtk_style_context_remove_class(sctx,classname);
}
/*----------------------------------------------------------------------------*/
gboolean on_resize(GtkWidget *widget,
		GtkAllocation *allocation, gpointer data) {
	char buff[128];
	char test[] = "#my1win { font-size: %.0fpx; }";
	my1gtkapp_t *gapp = (my1gtkapp_t*) data;
	snprintf(buff,128,test,((float)allocation->width*12.0/450));
	gtk_css_provider_load_from_data(gapp->css_more,buff, -1, NULL);
/*
	printf("Resize: %d x %d (%s)\n",allocation->width,allocation->height,buff);
*/
	return TRUE;
}
/*----------------------------------------------------------------------------*/
gboolean on_key_press(GtkWidget *widget, GdkEventKey *kevent,
		gpointer data) {
	if(kevent->type == GDK_KEY_PRESS) {
		/** g_message("%d, %c", kevent->keyval, kevent->keyval); */
		if(kevent->keyval == GDK_KEY_Escape|| kevent->keyval == GDK_KEY_q) {
			gtk_main_quit();
			return TRUE;
		}
	}
	return FALSE;
}
/*----------------------------------------------------------------------------*/
void on_button_heal_clicked(GtkWidget *widget,my1gtkapp_t* gapp) {
	char *mesg, *what;
	if (gapp->flag&FLAG_HEAL) {
		mesg = "Hello, World!";
		what = "Heal";
		gapp->flag &= ~FLAG_HEAL;
	}
	else {
		mesg = "Heal! Make it a better place!";
		what = "Back";
		gapp->flag |= FLAG_HEAL;
	}
	gtk_label_set_text((GtkLabel*)gapp->show,mesg);
	gtk_button_set_label(GTK_BUTTON(gapp->heal),what);
}
/*----------------------------------------------------------------------------*/
void gtkapp_build_gui(my1gtkapp_t* gapp, char* glade_xml) {
	GdkDisplay *disp;
	GdkScreen *gscr;
	GtkBuilder *builder;
	/* build interface (glade) */
	builder = gtk_builder_new();
	gtk_builder_add_from_string(builder,glade_xml,-1,0x0);
	/* get main window */
	gapp->main = GTK_WIDGET(gtk_builder_get_object(builder,"window_main"));
	gtk_widget_set_name(gapp->main,"my1win");
	gapp->show = GTK_WIDGET(gtk_builder_get_object(builder,"label_hello"));
	gapp->done = GTK_WIDGET(gtk_builder_get_object(builder,"button_ok"));
	gapp->heal = GTK_WIDGET(gtk_builder_get_object(builder,"button_heal"));
	/* connect signals defined in glade - most my app dont need this */
	gtk_builder_connect_signals(builder,0x0);
	/* free build resources */
	g_object_unref(builder);
	/* dynamic font size! */
	gapp->css_main = gtk_css_provider_new();
	disp = gdk_display_get_default();
	gscr = gdk_display_get_default_screen(disp);
	gtk_style_context_add_provider_for_screen(gscr,
		GTK_STYLE_PROVIDER(gapp->css_main),
		GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	gtk_css_provider_load_from_data(gapp->css_main,
		"#my1win { font-size: 18px; }\n"
		".border_bt { border-bottom-width: 1px;\n"
		"  border-bottom-style: solid;\n"
		"  border-top-width: 1px;\n"
		"  border-top-style: solid; }\n"
		".border_lr { border-left-width: 1px;\n"
		"  border-left-style: solid;\n"
		"  border-right-width: 1px;\n"
		"  border-right-style: solid; }\n"
		".font_double { font-size: 2em; }\n"
		".font_header { font-size: 1.2em; color: blue; }", -1, NULL);
	gapp->css_more = gtk_css_provider_new();
	gtk_style_context_add_provider_for_screen(gscr,
		GTK_STYLE_PROVIDER(gapp->css_more),
		GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
	g_signal_connect(G_OBJECT(gapp->main),"size-allocate",
		G_CALLBACK(on_resize),(gpointer)gapp);
	g_signal_connect(G_OBJECT(gapp->main),"key_press_event",
		G_CALLBACK(on_key_press),(gpointer)gapp);
	g_signal_connect(G_OBJECT(gapp->heal),"clicked",
		G_CALLBACK(on_button_heal_clicked),(gpointer)gapp);
	/* show/start main interface */
	gtk_widget_show(gapp->main);
	/**gtk_window_fullscreen(GTK_WINDOW(gapp->main));*/
	/* prepare stuff here */
	gtk_button_set_label(GTK_BUTTON(gapp->done),"Done");
}
/*----------------------------------------------------------------------------*/
/* these are defined in glade! */
void on_window_main_destroy(void) {
	gtk_main_quit();
}
/*----------------------------------------------------------------------------*/
void on_button_ok_clicked(void) {
	gtk_main_quit();
}
/*----------------------------------------------------------------------------*/
